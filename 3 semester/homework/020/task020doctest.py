import doctest
from task007 import toArab

def f(x):
    """
    >>> f('IIIIX')
    'Incorrect roman number'
    """
    return toArab(x)

if __name__ == '__main__':
    doctest.testmod()
