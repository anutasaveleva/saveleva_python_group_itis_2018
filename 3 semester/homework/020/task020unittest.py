import unittest
from task007 import toArab


class TestFunction(unittest.TestCase):

    def setUp(self):
        self.x = 'XVII'

    def test_sample(self):
        self.assertEqual(toArab(self.x), 17)

    def tearDown(self):
        self.x = ''


if __name__ == '__main__':
    unittest.main()