from task007 import toArab
import pytest

@pytest.fixture(scope='module')
def obj():
    class A:
        def __init__(self):
            self.x = 'XXVII'
    return A()

def test2(obj):
    obj.x += 'I'
    assert 28 == toArab(obj.x)

def test3(obj):
    obj.x += 'I'
    assert 'Incorrect roman number' == toArab(obj.x)