def roman_deco(f):
    def wrapper():
        s = ''
        dig = f()
        if isinstance(dig, int):
            counter = dig // 1000
            s += counter * 'M'
            dig %= 1000

            counter = dig // 500
            if dig >= 900:
                s += 'CM'
                dig %= 900
            else:
                s += counter * 'D'
                dig %= 500

            counter = dig // 100
            if counter > 3:
                s += 'CD'
            else:
                s += counter * 'C'
            dig %= 100

            counter = dig // 50
            if dig >= 90:
                s += 'XC'
                dig %= 90
            else:
                s += counter * 'X'
                dig %= 50

            counter = dig // 10
            if dig >= 40:
                s += 'XL'
            else:
                s += counter * 'X'
            dig %= 10

            counter = dig // 5
            if dig == 9:
                s += 'IX'
                dig = 0
            else:
                s += counter * 'V'
            dig %= 5

            if dig > 3:
                s += 'IV'
            else:
                s += dig * 'I'
        return s

    return wrapper

@roman_deco
def f():
    return 519


print(f())
