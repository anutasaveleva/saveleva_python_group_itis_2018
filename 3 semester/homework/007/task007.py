import math

d = {'X': 10, 'I': 1, 'L': 50, 'C': 100, 'D': 500, 'M': 1000, 'V': 5}
s = list(input().upper())
prev = 0
arab = 0
sameChrs = 1
maximum = 0
isIncorrect = False

for i in range(len(s) - 1, -1, -1):
    if s[i] in d and sameChrs < 4 and not isIncorrect:
        cur = d[s[i]]
        if cur < prev:
            arab -= cur
        else:
            if cur == prev:
                sameChrs += 1
                if (cur == 5 or cur == 50 or cur == 500):
                    isIncorrect = True
                    break
            else:
                sameChrs = 1

            arab += cur
            maximum = max(maximum, sameChrs)
        prev = cur
    else:
        isIncorrect = True
        break
if arab < 0 or maximum > 3 or isIncorrect:
    print('Incorrect roman number')
else:
    print(arab)
