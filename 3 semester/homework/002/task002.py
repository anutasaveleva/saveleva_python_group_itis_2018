import math

class Vector2D:
    def __init__(self, xCoord=0, yCoord=0):
        self.x = xCoord
        self.y = yCoord

    def __add__(self, other):
        return Vector2D(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Vector2D(self.x - other.x, self.y - other.y)

    def __str__(self):
        return "<%s, %s>" % (self.x, self.y)

    def len(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def __abs__(self):
        return self.len()

    def __mul__(self, other):
        if isinstance(other, int):
            return Vector2D(self.x * other, self.y * other)
        return Vector2D(self.x * other.x, self.y * other.y)

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y