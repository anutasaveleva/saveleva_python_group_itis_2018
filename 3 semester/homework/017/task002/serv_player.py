import random
import socket


def client_program():
    port = 5000
    name = input("Enter your name: ")
    hp = 5
    serv_socket = socket.socket()
    serv_socket.bind(('localhost', port))
    serv_socket.listen(1)
    conn, addr = serv_socket.accept()
    #enemyname = serv_socket.recv(1024).decode()
    #serv_socket.send(name.encode())
    #message = input("Enter force to punch: ")

    while hp > 0:

        print("enemy's turn..")
        data = conn.recv(1024).decode()  # receive response
        if not data:
            break
        if not str.isalpha(data) and conn:
            rand = random.random()
            if (rand < (1 - 1.0 * int(data) / 10)):
                hp -= int(data)
                if hp <= 0:
                    print('you lost')
                    conn.send('l'.encode())

                else:
                    print("Punch was made! %s's HP is %s" % (name, hp))
            else:
                print("Missed!")


        else:
            print('you won')
            break
        message = input("Enter force to punch: ")
        conn.send(message.encode())  # send message

    serv_socket.close()  # close the connection


if __name__ == '__main__':
    client_program()
