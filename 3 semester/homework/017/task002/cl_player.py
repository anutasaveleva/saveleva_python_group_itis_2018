import random
import socket


def client_program():
    port = 5000  # socket server port number

    client_socket = socket.socket()  # instantiate
    client_socket.connect(('localhost', port))  # connect to the server
    name = input("Enter your name: ")
    #client_socket.send(name.encode())
    hp = 5
    message = input("Enter force to punch: ")
    while hp > 0:
        client_socket.send(message.encode())  # send message
        print('waiting for enemy...')
        data = client_socket.recv(1024).decode()  # receive response
        if not str.isalpha(data):
            rand = random.random()
            if (rand < (1 - 1.0 * int(data) / 10)):
                hp -= int(data)
                if hp <= 0:
                    print('you lost')
                    client_socket.send('l'.encode())
                    break
                else:
                    print("Punch was made! %s's HP is %s" % (name, hp))
            else:
                print("Missed!")
            message = input("Enter force to punch: ")  # again take input
        else:
            print('won')
            break


    client_socket.close()  # close the connection


if __name__ == '__main__':
    client_program()
