class EvenNumber:
    def __init__(self, x):
        if self.checker(x):
            self.x = int(x)
        else:
            raise Exception('not even number')

    def checker(self, x):
        if ((isinstance(x, int) or (isinstance(x, float) and x % 1 == 0) or isinstance(x, str)) \
            and int(x) % 2 == 0) or (isinstance(x, EvenNumber) and int(x.x) % 2 == 0):
            return True
        else:
            raise Exception('the value is not even')

    def __truediv__(self, other):
        raise Exception('not operation of an even  number')

    def __mul__(self, other):
        if not isinstance(other, EvenNumber):
            other = EvenNumber(other)
        return EvenNumber(self.x * other.x)

    def __radd__(self, other):
        if not isinstance(other, EvenNumber):
            other = EvenNumber(other)
        return EvenNumber(self.x + other.x)

    def __add__(self, other):
        if not isinstance(other, EvenNumber):
            other = EvenNumber(other)
        return EvenNumber(self.x + other.x)

    def __sub__(self, other):
        if not isinstance(other, EvenNumber):
            other = EvenNumber(other)
        return EvenNumber(self.x - other.x)

    def __rsub__(self, other):
        if not isinstance(other, EvenNumber):
            other = EvenNumber(other)
        return EvenNumber(self.x - other.x)

    def __str__(self):
        return str(self.x)


x = EvenNumber(3 - 1)

y = EvenNumber(4.0)
z = 4
w = EvenNumber('8')
print(x * z)
print(x - w)


class MyType:
    def __init__(self, type):
        self.list = list()
        self.t = type

    def append(self, x):
        if (isinstance(x, self.t)):
            self.list.append(x)
        else:
            raise Exception('Type of variable should be %s' % self.t)

    def __str__(self):
        return self.list

    def __len__(self):
        return len(self.list)

    def __iter__(self):
        return self.list.__iter__()


m = MyType(str)
n = 'e'
m.append(n)
m.append('xxx')
m.append(3)
for item in m:
    print(item)
