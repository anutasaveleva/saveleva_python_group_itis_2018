from django.http import HttpResponse, HttpResponseRedirect

from .models import Tweet
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators import csrf
from django.views.decorators.csrf import csrf_protect
from django.utils import timezone
from django.contrib import auth
from django.contrib.auth.decorators import login_required

def index(request):
    tweet_list = Tweet.objects.order_by('-pub_date')

    context = {'tweet_list': tweet_list}
    if request.user.is_authenticated:
        context.update({'authorized':'T'})
    else:
        context.update({'unauthorized':'T'})

    if request.method == 'POST':
        if request.POST.get('tweet'):
            tweet = Tweet()
            tweet.tweet_text = request.POST.get('tweet')
            tweet.pub_date = timezone.now()
            tweet.username = request.user.username
            tweet.save()
        return HttpResponseRedirect(reverse('index'))

    else:
        if request.GET.get('usertweet'):
            return HttpResponseRedirect(reverse('posts', args=[request.user.username]))
        elif request.GET.get('logout'):
            return HttpResponseRedirect(reverse('login'))
        elif request.GET.get('login'):
            return HttpResponseRedirect(reverse('login'))
        else:
            return render(request, 'tweets/tweet.html', context)

@login_required(login_url='/login/')
def posts(request, username):
    tweet_list = Tweet.objects.filter(username=username).order_by('-pub_date')
    context = {'tweet_list': tweet_list}
    return render(request, 'tweets/posts.html', context)