from django.db import models


class Tweet(models.Model):
    tweet_text = models.CharField(max_length=140)
    pub_date = models.DateTimeField('date published')
    username = models.CharField(max_length=40)

    def __str__(self):
        return self.tweet_text