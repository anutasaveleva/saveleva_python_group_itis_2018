import requests
import os
from bs4 import BeautifulSoup, SoupStrainer

url = 'https://career.ru/search/vacancy?clusters=true&enable_snippets=true&specialization=15.93&area=1624&from=cluster_area'
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
}
search = ['Требования', 'ищем']
response = requests.get(url, headers=headers)
soup = BeautifulSoup(response.content, 'html.parser')
professions = soup.find_all('div', {'class': 'vacancy-serp-item '})
for profession in professions:
    name = profession.find('a')
    print(name.text)
    link = profession.find('a').get('href')
    linkresponse = requests.get(link, headers=headers)
    soup2 = BeautifulSoup(linkresponse.content, 'html.parser')
    requirements = soup2.find_all('div', {'class': 'g-user-content'})
    soup3 = BeautifulSoup(linkresponse.content, 'html.parser')
    requirements2 = soup3.find_all('div', {'class': 'vacancy-branded-user-content'})
    for r in requirements:
        n = r.find_all('ul')
        if len(n) > 1:
            print('Requirements: ', n[1].text)

    else:
        for r in requirements2:
            n = r.find_all('ul')
            if len(n) > 1:
                print('Requirements: ', n[-1].text)
