import requests
import os

from bs4 import BeautifulSoup, SoupStrainer

url = 'https://ru.wikipedia.org/wiki/%D0%A7%D0%B5%D0%B1%D0%BE%D0%BA%D1%81%D0%B0%D1%80%D1%8B'
file_type = '.pdf'

response = requests.get(url)
for link in BeautifulSoup(response.content, 'html.parser', parse_only=SoupStrainer('a')):
    if link.has_attr('href'):
        if file_type in link['href']:
            full_path = url + link['href']
            with open('%s' % (full_path.split('/')[-1]), 'wb') as f:
                f.write(requests.get(full_path).content)

