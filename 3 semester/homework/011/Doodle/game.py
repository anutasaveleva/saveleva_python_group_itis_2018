import pygame
from pygame.sprite import collide_rect
from doodler import Doodler
from flying_platform import Platform
import random


class DoodleJump:
    pygame.init()
    pygame.display.set_caption("Doodle Jump")
    platforms = []  # координаты платформ
    window_size = 700
    background_color = (0, 220, 255)

    def __init__(self):
        self.window = pygame.display.set_mode((self.window_size, self.window_size))
        self.platforms = [(600, self.window_size)]
        self.platform = pygame.image.load("static/platform_mini.png")
        self.doodler = Doodler()
        self.run = True

    def draw_platforms(self):
        for platform in self.platforms:
            a = random.randint(0, self.window_size)
            if platform[1] > -50000:
                self.window.blit(self.platform, (platform[0], platform[1] - self.doodler.cameray))
        self.platforms.append((a, self.platforms[-1][1] - 30))

        self.platforms.append((random.randint(0, self.window_size), self.platforms[-1][1] - 100))
        if self.platforms[1][1] - self.doodler.cameray > 900:
            self.platforms.pop(0)

    def update_platforms(self):
        player = pygame.Rect(self.doodler.x,
                             self.doodler.y,
                             self.doodler.image.get_width(),
                             self.doodler.image.get_height()-10)

        for platform in self.platforms:
            rect = pygame.Rect(platform[0], platform[1], self.platform.get_width(), self.platform.get_height())\

            if rect.colliderect(player) and rect.collidepoint(player.x, player.bottom-10) and self.doodler.gravity \
                    and self.doodler.y < platform[1] - self.doodler.cameray:
                self.doodler.jump = 15
                self.doodler.gravity = 0

    def is_death(self):
        if self.doodler.y - self.doodler.cameray > 1000:
            self.platforms = [(0, -100)]
            print("Умер")
            pygame.quit()

    def main(self):
        while self.run:
            if self.doodler.y - self.doodler.cameray < 1000:
                self.update_platforms()
            self.draw_platforms()
            self.window.blit(self.doodler.image, (self.doodler.x, self.doodler.y - self.doodler.cameray))
            self.doodler.update()
            pygame.display.update()
            pygame.time.delay(30)
            self.window.fill(self.background_color)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.run = False
            self.is_death()

    pygame.quit()


DoodleJump().main()
