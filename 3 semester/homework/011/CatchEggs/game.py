# <editor-fold desc="Основа">
import pygame
import time
import random
import datetime
from settings import *
from pygame.locals import *


# <editor-fold desc="Функции">
def add_eggs(eggs):
    egg = Egg(eggColor, egg_width, egg_height)
    egg.rect.x = random.choice(xCoord)
    egg.rect.y = random.choice(yCoord)
    egg.xc = egg.rect.x
    egg.yc = egg.rect.y
    eggs.add(egg)


def draw_wolf_where_needed():
    if wolfIsTopLeft:
        screen.blit(left_top_wolf, wolfLeft)
    if wolfIsTopRight:
        screen.blit(right_top_wolf, wolfRight)
    if wolfIsBottomLeft:
        screen.blit(left_bottom_wolf, wolfLeft)
    if wolfIsBottomRight:
        screen.blit(right_bottom_wolf, wolfRight)


def draw_background_in_game():
    screen.blit(sky, (initx, inity))
    screen.blit(grass, (initx, edgey))
    screen.blit(wood1, (initx, upyu))
    screen.blit(wood1, (rightx, upyu))
    screen.blit(wood2, (initx, downyu))
    screen.blit(wood2, (initx, downyu))
    screen.blit(wood2, (rightx, downyu))
    screen.blit(wood2, (rightx, downy))
    screen.blit(wood3, (leftx, downyu))
    screen.blit(wood3, (leftx, downy))
    screen.blit(wood3_2, (midx, downyu))
    screen.blit(wood3_2, (midx, downy))
    score = fontMedium.render('Score: ' + str(currentScore), True, white)
    lifes = fontMedium.render('Lifes: ' + str(currentLifes), True, white)

    screen.blit(chicken1, (chickx, chickyu))  # верхняя левая курица
    screen.blit(chicken1, (chickx, chickyd))  # нижняя левая курица
    screen.blit(chicken2, (rightx, chickyu))  # верхняя правая курица
    screen.blit(chicken2, (rightx, chickyd))  # нижняя правая курица
    screen.blit(score, (scorex, scorey))  # Score
    screen.blit(lifes, (scorex, scorey + 30))  # Lifes
    screen.blit(grass, (initx, edgey))


def draw_line(color):
    pygame.draw.line(screen, color, (lxs, y), (lxs + 50, y), 3)
    pygame.draw.line(screen, color, (rxs, y), (rxs + 50, y), 3)


def draw_lines(color1, color2, color3):
    global y
    draw_line(color1)  # play
    y += 50
    draw_line(color2)  # rules
    y += 50
    draw_line(color3)  # records
    y -= 100


def draw_frame():
    pygame.draw.line(screen, white, (10, 10), (590, 10), 3)
    pygame.draw.line(screen, white, (10, 10), (10, 390), 3)
    pygame.draw.line(screen, white, (10, 390), (590, 390), 3)
    pygame.draw.line(screen, white, (590, 10), (590, 390), 3)


def make_start_menu():
    global running, menuFirstTime
    if menuFirstTime:
        pygame.mixer.music.load('songs/metal.mp3')
        pygame.mixer.music.set_volume(0.3)
        pygame.mixer.music.play(-1, 0.0)
        menuFirstTime = False
    on_play_button = True
    on_about_button = False
    on_records_button = False
    screen.fill(black)
    pygame.draw.rect(screen, frameColor, ((100, 40), (400, 15)))
    pygame.draw.rect(screen, frameColor, ((100, 40), (15, 100)))
    pygame.draw.rect(screen, frameColor, ((100, 125), (400, 15)))
    pygame.draw.rect(screen, frameColor, ((485, 40), (15, 100)))
    draw_line(white)
    draw_frame()

    screen.blit(fontBig.render('волк ловит яйца', True, brown), wolfcatch)
    screen.blit(fontSmall.render('играть', True, brown), playc)
    screen.blit(fontSmall.render('правила', True, brown), rulesc)
    screen.blit(fontSmall.render('рекорды', True, brown), recordsc)
    screen.blit(fontSuperSmall.render('2018', True, brown), numc)
    pygame.display.flip()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

        # <editor-fold desc="Обработка нажатия клавиш">
        pressed_list = pygame.key.get_pressed()
        if pressed_list[pygame.K_UP]:
            if on_about_button:
                beep.play()
                if flag_up:
                    draw_lines(white, black, black)
                    on_play_button = True
                    on_about_button = False
                    on_records_button = False
                    pygame.display.flip()
                flag_up = False
            if on_records_button:
                beep.play()
                if flag_up:
                    draw_lines(black, white, black)
                    on_play_button = False
                    on_about_button = True
                    on_records_button = False
                    pygame.display.flip()
                flag_up = False
        else:
            flag_up = True

        if pressed_list[pygame.K_DOWN]:
            if on_play_button:
                beep.play()
                if flag_down:
                    draw_lines(black, white, black)
                    on_play_button = False
                    on_about_button = True
                    on_records_button = False
                    pygame.display.flip()
                flag_down = False
            if on_about_button:
                beep.play()
                if flag_down:
                    draw_lines(black, black, white)
                    on_play_button = False
                    on_about_button = False
                    on_records_button = True
                    pygame.display.flip()
                flag_down = False
        else:
            flag_down = True

        if pressed_list[pygame.K_SPACE] or pressed_list[pygame.K_RETURN]:
            if flag_space_return:
                if on_play_button:
                    beep.play()
                    pygame.mixer.music.load('songs/ofuzake.mp3')
                    pygame.mixer.music.set_volume(0.3)
                    pygame.mixer.music.play(-1, 0.0)
                    ticks.tick(FPS)
                    return
                if on_about_button:
                    beep.play()
                    screen.fill(black)
                    make_about_menu()
                    break
                if on_records_button:
                    beep.play()
                    screen.fill(black)
                    make_records_menu()
                    break
            flag_space_return = False
        else:
            flag_space_return = True
        # </editor-fold>


def make_about_menu():
    global running, isFirstTime
    draw_frame()
    screen.blit(fontBig.render('правила', True, brown), (rulesx, upyu))
    file = open("instructions.txt")
    z = 120
    lines = file.read().split(r'\n')
    for line in lines:
        z += 18
        screen.blit(fontSuperSmall.render(line, True, white), (30, z))
    pygame.display.flip()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                isFirstTime = False
                running = False
                return False

        pressed_list = pygame.key.get_pressed()

        if pressed_list[pygame.K_ESCAPE]:
            if flag_escape:
                beep.play()
                screen.fill(black)
                make_start_menu()
                break
            flag_escape = False
        else:
            flag_escape = True


def make_records_menu():
    global running, isFirstTime
    draw_frame()
    screen.blit(fontBig.render('Рекорды', True, brown), (rulesx, upyu))
    file = open("records.txt")
    z = 120
    counter = 1
    point_start_x = 230
    lines = file.readlines()
    for line in lines:
        z += 22
        if counter == 10:
            screen.blit(fontSmall.render(str(counter), True, white), (194, z))
        else:
            screen.blit(fontSmall.render(str(counter), True, white), (xfedge, z))
        counter += 1
        for i in range(16):
            screen.blit(fontSmall.render('.', True, white), (point_start_x, z))
            point_start_x += 10
        point_start_x = 225
        screen.blit(fontSmall.render(line, True, white), (390, z))
    pygame.display.flip()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                isFirstTime = False
                running = False
                return False

        pressed_list = pygame.key.get_pressed()

        if pressed_list[pygame.K_ESCAPE]:
            if flag_escape:
                beep.play()
                screen.fill(black)
                make_start_menu()
                break
            flag_escape = False
        else:
            flag_escape = True


def make_game_over_menu():
    global running, records
    if currentScore > records[9]:
        records[9] = currentScore
        records.sort()
        records.reverse()
        file_to_write = open('records.txt', 'w')
        for record in records:
            file_to_write.write(str(record) + '\n')
        file_to_write.close()
    game_over.play()
    on_play_button = True
    on_menu_button = False
    screen.fill(black)
    draw_frame()

    screen.blit(fontBig.render('Конец игры', True, brown), gameoverc)
    screen.blit(fontMedium.render('Score: ' + str(currentScore), True, white), scorecoord)
    draw_line(white)
    screen.blit(fontSmall.render('заново', True, brown), againc)
    screen.blit(fontSmall.render('главное меню', True, brown), mainc)
    pygame.display.flip()
    eggs.empty()
    timer = time.clock() + 15
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

        # <editor-fold desc="Обработка нажатия клавиш">
        pressed_list = pygame.key.get_pressed()
        if pressed_list[pygame.K_UP] and on_menu_button:
            beep.play()
            if flag_up:
                draw_lines(white, black, black)
                on_play_button = True
                on_menu_button = False
                pygame.display.flip()
            flag_up = False
        else:
            flag_up = True
        if pressed_list[pygame.K_DOWN] and on_play_button:
            beep.play()
            if flag_down:
                draw_lines(black, white, black)
                on_play_button = False
                on_menu_button = True
                pygame.display.flip()
            flag_down = False
        else:
            flag_down = True
        if pressed_list[pygame.K_SPACE] or pressed_list[pygame.K_RETURN]:
            if flag_space_return:
                if on_play_button:
                    running = True
                    pygame.mixer.music.play(-1, 0.0)
                    time.sleep(0.3)
                    return
                if on_menu_button:
                    pygame.mixer.music.load('songs/metal.mp3')
                    pygame.mixer.music.set_volume(0.3)
                    pygame.mixer.music.play(-1, 0.0)
                    time.sleep(0.15)  # опять же единственный выход, который я вижу, чтобы не было коллизий с кнопками
                    make_start_menu()
                    break
            flag_space_return = False
        else:
            flag_space_return = True
        if pressed_list[pygame.K_ESCAPE]:
            if flag_esc:
                running = False
                return
            flag_esc = False
        else:
            flag_esc = True
        # </editor-fold>


def pause():
    paused = True
    while paused:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    paused = False
        screen.fill(black)
        screen.blit(fontSmall.render('пауза', True, brown), (262, yedge))
        screen.blit(fontSmall.render('для продолжения нажмите клавишу ПРОБЕЛ', True, white), (26, 230))
        pygame.display.update()
    timer = time.clock()


def key_handler(pressed_list):
    global wolfIsTopRight, wolfIsTopLeft, wolfIsBottomLeft, wolfIsBottomRight
    if pressed_list[K_DOWN] and wolfIsTopRight:
        wolfIsBottomRight = True
        wolfIsTopRight = False
    if pressed_list[K_DOWN] and wolfIsTopLeft:
        wolfIsTopLeft = False
        wolfIsBottomLeft = True
    if pressed_list[K_UP] and wolfIsBottomRight:
        wolfIsBottomRight = False
        wolfIsTopRight = True
    if pressed_list[K_UP] and wolfIsBottomLeft:
        wolfIsBottomLeft = False
        wolfIsTopLeft = True
    if pressed_list[K_RIGHT]:
        if wolfIsTopLeft:
            wolfIsTopLeft = False
            wolfIsTopRight = True
        elif wolfIsBottomLeft:
            wolfIsBottomLeft = False
            wolfIsBottomRight = True
        elif wolfIsTopRight or wolfIsTopLeft or wolfIsBottomRight or wolfIsBottomLeft == False:
            wolfIsTopRight = True
    if pressed_list[K_LEFT]:
        if wolfIsTopRight:
            wolfIsTopRight = False
            wolfIsTopLeft = True
        elif wolfIsBottomRight:
            wolfIsBottomRight = False
            wolfIsBottomLeft = True
        elif wolfIsTopRight or wolfIsTopLeft or wolfIsBottomRight or wolfIsBottomLeft == False:
            wolfIsTopLeft = True
    if pressed_list[K_p]:
        pause()


# </editor-fold>

# <editor-fold desc="Классы">
class Egg(pygame.sprite.Sprite):

    def __init__(self, color, width, height):
        super().__init__()
        self.image = pygame.Surface([width, height])
        pygame.draw.ellipse(self.image, color, [0, 0, width, height])
        self.image.set_colorkey(black)
        self.rect = self.image.get_rect()
        self.xc = 0.0
        self.yc = 0.0
        self.move_time = 0.005

    def become_caught(self):
        global currentScore, fromUpper, toastyTime, toastyClockStart, rabbit
        egg_caught.play()
        currentScore += 1
        if currentScore % 13 == 0 and currentScore > 0:
            toasty.play()
            toastyClockStart = 200
            rabbit.reset_pos()
            toastyTime = True
        s = "I'm in!"
        fromUpper = False
        instructions = pygame.font.SysFont("Times New Roman", 15)
        if self.rect.x < 210:
            self.xc += 140
        else:
            self.xc -= 160
        screen.blit(instructions.render(s, True, green), (self.xc, self.yc + 21))
        pygame.display.update()
        if currentScore % 3 == 0 and self.move_time > 0:
            self.move_time -= 0.001
        if self.move_time < 0.002:
            self.move_time = 0
        time.sleep(self.move_time)
        self.rect.x = random.choice(xCoord)
        self.rect.y = random.choice(yCoord)
        self.xc = self.rect.x
        self.yc = self.rect.y
        fromUpper = False

    def update(self):
        global currentLifes, currentScore, fromUpper
        shelf_finished = False
        if self.rect.x < xfedge or self.rect.x > xsedge:
            if self.rect.x < xfedge + 10:
                self.rect.x += 1
                x_fin = self.xc + 125
            else:
                self.rect.x -= 1
                x_fin = self.xc - 125
            y_fin = self.yc + 21
            a = (y_fin - self.yc) / (x_fin - self.xc)
            b = self.yc - a * self.xc
            self.rect.y = a * self.rect.x + b

        else:
            shelf_finished = True
            if xfedge + 20 > self.rect.x >= xfedge - 10 and self.rect.y < yedge:
                if wolfIsTopLeft:
                    self.become_caught()
                else:
                    fromUpper = True
            elif xredge <= self.rect.x <= xredge + 25 and self.rect.y < yedge:
                if wolfIsTopRight:
                    self.become_caught()
                else:
                    fromUpper = True

            elif (xfedge + 20 > self.rect.x >= xfedge - 10 and wolfIsBottomLeft and \
                    yuedge + 15 > self.rect.y >= yuedge - 10 or \
                    xredge <= self.rect.x <= xredge + 25 and wolfIsBottomRight and \
                  yuedge + 15 > self.rect.y >= yuedge - 10)and not fromUpper :
                self.become_caught()

        if shelf_finished:
            if self.rect.y < 300 and shelf_finished:
                self.rect.y += 10
            if self.rect.y >= 300:
                egg_cracking.play()
                s = "Crack!"
                fromUpper = False
                instructions = pygame.font.SysFont("Times New Roman", 18)
                currentLifes -= 1
                screen.blit(instructions.render(s, True, eggColor), (self.rect.x, self.rect.y))
                pygame.display.update()
                self.rect.x = self.xc = random.choice(xCoord)
                self.rect.y = self.yc = random.choice(yCoord)
        time.sleep(self.move_time)


class Rabbit(pygame.sprite.Sprite):

    def __init__(self):
        self.image = pygame.transform.scale(pygame.image.load('images/toasty.png').convert_alpha(), (180, 119))
        self.image.set_colorkey(white)
        self.rect = self.image.get_rect()

    def draw(self):
        screen.blit(self.image, (self.rect.x, self.rect.y))

    def update(self):
        if self.rect.x < 0:
            self.rect.x += 3.5

    def reset_pos(self):
        self.rect.x = -170
        self.rect.y = 280


# </editor-fold>

fileToRead = open('records.txt')
lines = fileToRead.readlines()
records = []
for line in lines:
    if line.strip():
        records.append(int(line))

rabbit = Rabbit()
add_eggs(eggs)
make_start_menu()

# <editor-fold desc="Игровой цикл">
while running and currentLifes > 0:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()

    draw_background_in_game()
    draw_wolf_where_needed()
    pressed_list = pygame.key.get_pressed()
    eggs.update()
    eggs.draw(screen)
    key_handler(pressed_list)

    if toastyTime and toastyClockStart > 0:
        rabbit.draw()
        rabbit.update()
        toastyClockStart -= 3

    pygame.display.update()

    if time.clock() - timer > 15 and len(eggs) < 4:
        timer = time.clock()
        add_eggs(eggs)

    if currentLifes == 0:
        pygame.mixer.music.stop()
        make_game_over_menu()
        if running and len(eggs) == 0:
            add_eggs(eggs)
        currentScore = 0
        currentLifes = 3
# </editor-fold>
