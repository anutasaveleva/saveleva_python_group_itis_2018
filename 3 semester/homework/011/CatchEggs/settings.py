# <editor-fold desc="Основа">
import pygame
import time
import random
import datetime

from pygame.locals import *

pygame.mixer.pre_init(44100, -16, 2, 2048)
pygame.mixer.init()
pygame.init()
size = [600, 400]
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Eggs Game")
# </editor-fold>

# <editor-fold desc="Переменные">
running = True
xCoord = (75.0, 515.0)
yCoord = (95.0, 150.0)
wolfLeft = (222, 130)
wolfRight = (245, 130)
fromUpper = False
x = 0.0
egg_width = 30
egg_height = 40
lxs = 140
rxs = 411
y = 208
instructions = True
wolfIsTopRight = False
wolfIsTopLeft = True
wolfIsBottomRight = False
wolfIsBottomLeft = False
isFirstTime = True
menuFirstTime = True
currentScore = 0
currentLifes = 3
frameColor = (240, 230, 140)
white = (255, 255, 255)
black = (0, 0, 0)
eggColor = (255, 255, 204)
brown = (210, 105, 30)
green = (0, 255, 0)
rulesx = 215
initx = 0
inity = 0
leftx = 67
rightx = 525
upyu = 75
downyu = 120
downy = 180
edgey = 270
midx = 385
chickx = 5
chickyu = 70
chickyd = chickyu + 60
scorex = 445
scorey = 10
xfedge = 200
xsedge = 380
yedge = 150
xredge = 375
yuedge = 200
wolfcatch = (124, 75)
playc = (258, 200)
rulesc = (250, 250)
recordsc = (248, 300)
numc = (282, 370)
gameoverc = (178, 75)
scorecoord = (238, 140)
againc = (256, 200)
mainc = (214, 250)
eggs = pygame.sprite.Group()
timer = time.clock()
fontBig = pygame.font.SysFont('Neighbor [RUS by Daymarius]', 35)
fontSmall = pygame.font.SysFont('Neighbor [RUS by Daymarius]', 20)
fontSuperSmall = pygame.font.SysFont('Neighbor [RUS by Daymarius]', 15)
fontMedium = pygame.font.SysFont('Neighbor [RUS by Daymarius]', 25)
inBag = False
toastyTime = False
toastyClockStart = 200
ticks = pygame.time.Clock()
FPS = 60

# картинки
chicken1 = pygame.transform.scale(pygame.image.load('images/chicken1.png').convert_alpha(), (69, 71))
chicken2 = pygame.transform.scale(pygame.image.load('images/chicken2.png').convert_alpha(), (69, 71))
left_top_wolf = pygame.transform.scale(pygame.image.load('images/left_top_wolf.png').convert_alpha(), (133, 204))
right_top_wolf = pygame.transform.scale(pygame.image.load('images/right_top_wolf.png').convert_alpha(), (133, 204))
left_bottom_wolf = pygame.transform.scale(pygame.image.load('images/left_bottom_wolf.png').convert_alpha(), (133, 204))
right_bottom_wolf = pygame.transform.scale(pygame.image.load('images/right_bottom_wolf.png').convert_alpha(),\
                                           (133, 204))
grass = pygame.transform.scale(pygame.image.load('images/grass.png').convert_alpha(), (600, 130))
wood1 = pygame.image.load('images/wood1.png')
wood2 = pygame.image.load('images/wood2.png')
wood3 = pygame.transform.rotate(pygame.image.load('images/wood3.png'), 80)
wood3_2 = pygame.transform.rotate(pygame.image.load('images/wood3_2.png'), 280)
sky = pygame.image.load('images/sky.png')

# звуковые эффекты
beep = pygame.mixer.Sound('sounds/beep.ogg')
game_over = pygame.mixer.Sound('sounds/game_over.wav')
egg_cracking = pygame.mixer.Sound('sounds/egg_cracking.wav')
egg_caught = pygame.mixer.Sound('sounds/egg_caught.ogg')
toasty = pygame.mixer.Sound('sounds/toasty.wav')
beep.set_volume(0.4)
game_over.set_volume(1.0)
egg_cracking.set_volume(0.2)
egg_caught.set_volume(0.2)
toasty.set_volume(1.0)

# </editor-fold>