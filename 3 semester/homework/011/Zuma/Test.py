import pygame

pygame.init()

height = 800
width = 800
zeroc = (0, 0)
chickc = (202, 650)
parrotc = (370, 650)
duckc = (538, 650)
size = (width, height)
screen = pygame.display.set_mode(size)
run = True
chick = pygame.image.load('pictures/chick.png')
duck = pygame.image.load('pictures/duck.png')
parrot = pygame.image.load('pictures/parrot.png')
letters_color = (25, 25, 112)
white = (255, 255, 255)
pygame.display.set_caption("Game")
fon = pygame.image.load('pictures/fon.jpg')
pygame.font.init()


def well_done_draw(score):
    font_acme_l = pygame.font.Font('Acme-Regular.ttf', 80)
    font_acme_m = pygame.font.Font('Acme-Regular.ttf', 50)
    player_score_st = 'You have %s points!' % score
    player_score_st_empty = 'You have %s point!' % score

    text_surface_well = font_acme_l.render('Well done!', True, letters_color)
    text_surface_point = font_acme_m.render(player_score_st, True, letters_color)
    text_surface_point_empty = font_acme_m.render(player_score_st_empty, True, letters_color)

    screen.fill(white)
    screen.blit(text_surface_well, (232, 200))
    screen.blit(chick, chickc)
    screen.blit(parrot, parrotc)
    screen.blit(duck, duckc)
    if (score == 0) or (score == -1) or (score == 1):
        screen.blit(text_surface_point_empty, (229, 400))
    else:
        screen.blit(text_surface_point, (190, 400))


def drow(score):
    run = True
    while run:
        for event in pygame.event.get():
            if event.type == pygame.QUIT or (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                run = False
        well_done_draw(score)
        pygame.display.flip()
