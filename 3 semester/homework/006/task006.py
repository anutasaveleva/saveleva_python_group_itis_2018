import os
import re


def frm(regex):
    removing_files = []
    for file in os.listdir(os.path.abspath(os.curdir)):
        if re.match(regex, file):
            removing_files.append(file)
    print(removing_files)
    if str.lower(input('Are you sure you need to delete these files? [Y/N]')) == 'y':
        for item in removing_files:
            os.remove(item)


while 1:
    print(os.path.abspath(os.curdir))
    string_arr = input().split()

    if not string_arr:
        continue
    else:
        cmd = string_arr[0]

    if cmd == 'exit':
        break
    elif cmd == 'rm':
        frm(string_arr[1])
    else:
        print('Unknown command %s' % cmd)

