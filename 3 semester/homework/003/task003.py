def checker(f):
    def wrapper(*args):
        for i in range(len(args)):
            if not isinstance(args[i], int):
                raise TypeError("Wrong type of %s parameter" % str(i+1))
        return f(*args)
    return wrapper

@checker
def s(*args):
    pass

s(2, 3)
s(4.0, 8)
s(1,23,4,5)
s(5, 1.1)