import os


class Superfile():
    def __init__(self, p):
        self.path = p
        if not os.path.exists(p):
            open(p, 'w+')

    def __add__(self, other):
        name = str(os.path.basename(self.path).split('.')[0])+str(os.path.basename(other.path))
        with open(name, 'w') as opened:
            opened.writelines(open(self.path, 'r').readlines())
            opened.writelines(open(other.path, 'r').readlines())
        return Superfile(name)

    def __mul__(self, dig):
        name = str.replace(self.path, '.', '%s.' % str(dig))
        initial = open(self.path, 'r')
        a = initial.readlines()
        initial.close()
        with open(name, 'w') as f:
            for i in range(dig):
                f.writelines(a)

    def println(self, s):
        with open(self.path, 'a') as toappend:
            toappend.write(s)


a = Superfile('file1.txt')
b = Superfile('file2.txt')
c = a+b+b
d = Superfile('file3.txt')
b*9
a.println('wooorld')
