d = {}
with open('eng_text.txt', 'r') as f:
    data = f.read()
    for letter in data:
        if str.isalpha(letter):
            l = letter.lower()
            if l not in d:
                d.setdefault(l, 1)
            else:
                d[l] += 1
for k, v in sorted(d.items()):
    print(k, v)
