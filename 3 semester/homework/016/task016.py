import os
import re
import requests
import shutil

p = os.path.abspath(os.curdir)
s = (os.path.abspath(os.curdir).split('\\'))

def fgrep(filename, pat, rec = 0):
    arr = []
    if os.path.isfile(filename):
        a = findLines(filename, pat)
        for item in a:
            arr.append(item)
    else:
        fcd(filename)
        files = os.listdir()
        for item in files:
            if len(findLines(item, pat)) > 0:
                arr.append(item)
            if os.path.isdir(item) and rec > 0:
                arr.append(fgrep(item, pat, 1))
        fcd('..')
    return arr


def fcd(path):
    global s
    p = os.path.abspath(os.curdir)
    if path != '..':
        try:
            if path.__contains__('\\'):
                os.chdir(path)
                s = path.split('\\')
            else:
                os.chdir(os.path.abspath(os.curdir) + '\\%s' % path)
                s.append(path)

        except FileNotFoundError:
            print('Path is not found.')

    else:
        if len(s) > 2:
            p = p.replace('\\%s' % s.pop(), '')
        elif len(s) == 2:
            p = p.replace(s.pop(), '')
        os.chdir(p)


def fappend(name):
    with open(name, 'a+') as f:
        inp = input('write whatever you need to add here: ')
        while inp:
            f.write(inp + '\n')
            inp = input()


def findMatches(regex, response):
    arr = []
    for item in re.finditer(regex, response.text):
        arr.append(item.group(1))
    return arr

def findLines(inner_cmd, pat):
    arr = []
    if os.path.isfile(inner_cmd):
        with open(inner_cmd, 'r') as opened:
            for line in opened.readlines():
                p = re.compile(pat)
                if len(p.findall(line)) != 0:
                    arr.append(line)

    return arr

while 1:
    print(os.path.abspath(os.curdir))
    string_arr = input().split()

    if not string_arr:
        continue
    else:
        cmd = string_arr[0]

    if cmd == 'exit':
        break
    elif cmd == 'dir':
        d = os.listdir('.')
        print('Содержимое папки %s' % p)
        for item in d:
            print(item)
        print('%d элементов' % len(d))
    elif cmd == 'cd':
        fcd(string_arr[1])

    elif cmd == 'append':
        fappend(string_arr[1])

    elif cmd == 'supercopy':
        for i in range(int(string_arr[2])):
            dst = string_arr[1].replace('.', '%s.' % str(i + 1))
            shutil.copyfile(string_arr[1], dst)

    elif cmd == 'wget':
        print('Starting download...')
        r = requests.get(string_arr[1])
        if len(string_arr) == 2:
            name = string_arr[1].split('/')[-1]
            with open('%s' % name, 'wb') as f:
                f.write(r.content)

        else:
            ext = string_arr[2]
            s = r"href=\"(http://[^\"]+[.]%s)\"" % ext
            for link in findMatches(s, r):
                name = link.split('/')[-1]
                with open('%s' % name, 'wb') as f:
                    f.write(requests.get(link).content)
        print('Done.')

    elif cmd == 'makenote':
        os.chdir('notes')
        notetext = ''
        if string_arr[1] == 'new':
            inp = input()
            while inp:
                notetext += inp
                inp = input()
            with open('%s.txt' % notetext[:10], 'w') as note:
                note.write(notetext)
        elif string_arr[1] == '-list':
            d = os.listdir('.')
            for item in d:
                print(item)
            print('%d заметок' % len(d))
        elif os.path.isfile(string_arr[1]):
            if len(string_arr) > 2:
                if string_arr[2] == 'edit':
                    with open(string_arr[1], 'a') as note:
                        note.write(input())
            else:
                with open(string_arr[1], 'r') as note:
                    print(note.readlines())
        os.chdir(p)
    elif cmd == 'grep':
        pat = string_arr[1]
        inner_cmd = string_arr[-1]
        gr = len(string_arr) - 3
        files = fgrep(inner_cmd, pat, gr)
        for name in files:
            print(str(name))

    else:
        print('Unknown command %s' % cmd)
