import pygame
import math
import time
from pygame.locals import *

width, height = 400, 400
play = True
step, step2 = 10, 10
rect_y = 30
x = 10
line_width = 7
start_pos = (10, 10)
blue = (0, 0, 255)
light_blue = (0, 255, 255)

pygame.init()
screen = pygame.display.set_mode((width, height))

while play and step < 300:
    for event in pygame.event.get():
        if event.type == QUIT:
            play = False

    pygame.draw.rect(screen, light_blue, (step2, rect_y, x, rect_y))
    if step2 < 300:
        step2 += 15
    step += 5
    pygame.draw.line(screen, blue, start_pos, (step, x), line_width)
    time.sleep(0.2)
    pygame.display.update()
