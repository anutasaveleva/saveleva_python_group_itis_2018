import pygame
import math
import time
from pygame.locals import *

x, y = 250, 250
size = 500
percent = 0.0
colour = (123, 123, 15)
black = (0, 0, 0)
alpha = 0
width = 6
play = True
circle_flag = True
r = 150
pygame.init()
pygame.font.init()
myfont = pygame.font.SysFont('Comic Sans MS', 30)
screen = pygame.display.set_mode((size, size))
rectX = (size - (r * 2)) / 2
rectY = (size - (r * 2)) / 2

while play:
    for event in pygame.event.get():
        if event.type == QUIT:
            play = False
    screen.fill(black)
    if alpha <= math.pi * 2:
        pygame.draw.arc(screen, colour, (rectX, rectY, r * 2, r * 2), 0, alpha, width)
        alpha += 0.05
        time.sleep(0.01)

    else:
        pygame.draw.circle(screen, colour, (x, y), r, width)
        time.sleep(0.3)
        if r == 150:
            r = 0
            width = 0
        else:
            r = 150
            width = 6

    percent = alpha * 100 / 6.28

    screen.blit(myfont.render('%s%%' % int(percent), True, colour), (230, 240))

    pygame.display.update()
