# VK Loading

import pygame
import time
from pygame.locals import *

pygame.init()
play = True
screen = pygame.display.set_mode((200, 200))
usual = (135, 206, 250)
dark = (72, 61, 139)


def setColors(n):
    colors = []
    colors.append(dark)
    for i in range(1, n):
        colors.append(usual)
    return colors


def changeColors():
    for i in range(len(colors)):
        if colors[i] == dark:
            colors[i] = usual
            if i == len(colors) - 1:
                colors[0] = dark
            else:
                colors[i + 1] = dark
            break


def drawRects(x, n):
    for i in range(n):
        pygame.draw.rect(screen, colors[i], (x, 30, 40, 20))
        x += 45


colors = setColors(3)
while play:
    screen.fill((255, 255, 255))
    for event in pygame.event.get():
        if event.type == QUIT:
            play = False
    startPoint = 15
    drawRects(startPoint, 3)
    changeColors()
    time.sleep(0.2)
    pygame.display.flip()
