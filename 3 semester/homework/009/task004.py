import pygame
import time
from pygame.locals import *


x = 200
y = 200
r = 30
white = (255, 255, 255)
purple = (150, 25, 225)
circle_flag = True
flag = True

pygame.init()
pygame.display.set_caption("Circle beating")
screen = pygame.display.set_mode((x*2, y*2))
while flag:
    for event in pygame.event.get():
        if event.type == QUIT:
            flag = False
    screen.fill(white)
    pygame.draw.circle(screen, purple, (x, y), r, 7)
    time.sleep(0.05)
    if r >= 100:
        circle_flag = False
    elif r <= 30:
        circle_flag = True
    if circle_flag:
        r += 5
    else:
        r -= 5
    pygame.display.update()
