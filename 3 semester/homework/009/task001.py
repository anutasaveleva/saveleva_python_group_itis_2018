import pygame
import math
import time
from pygame.locals import *

width, height = 250, 250
points = [[-15, 40], [-15, 15], [0, 0], [15, 15], [15, 40]]
play = True
green = (0, 250, 0)

pygame.init()
pygame.display.set_caption("Sand clock")
screen = pygame.display.set_mode((width, height))


def draw(points, angle, reversed=False):
    new_p = []
    for item in points:
        if reversed:
            item[1] *= -1
        x = item[0] * math.cos(angle) + item[1] * math.sin(angle)
        y = -item[0] * math.sin(angle) + item[1] * math.cos(angle)
        new_p.append([x + 125, y + 125])
    pygame.draw.lines(screen, green, True, new_p, 5)


alpha = 0
while play:
    screen.fill((255, 255, 255))
    for event in pygame.event.get():
        if event.type == QUIT:
            play = False
    draw(points, alpha)
    draw(points, alpha, True)
    time.sleep(0.5)
    alpha -= math.pi / 6
    pygame.display.update()
