import pygame
import math
import time
from pygame.locals import *

x, y = 250, 250
size = 500
percent = 0.0
newX, newY = 0.0, 0.0
colour = (123, 123, 15)
black = (0, 0, 0)
curcol = colour
alpha = 0
width = 6
play = True
circle_flag = True
r = 150
pygame.init()
pygame.font.init()
myfont = pygame.font.SysFont('Comic Sans MS', 30)
screen = pygame.display.set_mode((size, size))
rectX = (size - (r * 2)) / 2
rectY = (size - (r * 2)) / 2

while play:
    for event in pygame.event.get():
        if event.type == QUIT:
            play = False
    screen.fill((0,0,0))
    pygame.draw.circle(screen, colour, (x, y), r, width)
    pygame.draw.arc(screen, black, (rectX, rectY, r * 2, r * 2), 0, alpha,width)
    if alpha >= math.pi * 2:
        black, colour = colour, black
        alpha = 0
    alpha += 0.05
    time.sleep(0.01)
    pygame.display.update()
