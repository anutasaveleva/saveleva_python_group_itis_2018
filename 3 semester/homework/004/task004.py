import os

s = (os.path.abspath(os.curdir).split('\\'))


def fcd(path):
    global s
    p = os.path.abspath(os.curdir)
    if path != '..':
        try:
            if path.__contains__('\\'):
                os.chdir(path)
                s = path.split('\\')
            else:
                os.chdir(os.path.abspath(os.curdir) + '\\%s' % path)
                s.append(path)

        except FileNotFoundError:
            print('Path is not found.')

    else:
        if len(s) > 2:
            p = p.replace('\\%s' % s.pop(), '')
        elif len(s) == 2:
            p = p.replace(s.pop(), '')
        os.chdir(p)


def fappend(name):
    with open(name, 'a+') as f:
        inp = input('write whatever you need to add here: ')
        while inp:
            f.write(inp + '\n')
            inp = input()


while 1:
    print(os.path.abspath(os.curdir))
    string_arr = input().split()

    if not string_arr:
        continue
    else:
        cmd = string_arr[0]

    if cmd == 'exit':
        break
    elif cmd == 'cd':
        fcd(string_arr[1])

    elif cmd == 'append':
        fappend(string_arr[1])
    else:
        print('Unknown command %s' % cmd)
