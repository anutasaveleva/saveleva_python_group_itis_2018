import time
from datetime import datetime, timedelta

f = open('timetable.txt')
weekdays = ('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat')


class ScheduleItem:
    def __init__(self, weekday, t, subj, teacher):
        self.weekday = weekday
        self.time = t
        self.subject = subj
        self.teacher = teacher


timetable = []
weekday = ""
for line in f:
    line = line.strip()
    if line in weekdays:
        weekday = line
        continue
    line_data = line.split('\t')
    timetable.append(ScheduleItem(weekday, line_data[0], line_data[1], line_data[2]))

def hours_per_week(timetable):
    stat = {}
    td = timedelta(milliseconds=0)
    for i in range(len(timetable)):
        delta = timedelta_creating(i, timetable)
        weekday = timetable[i].weekday
        if weekday in stat:
            stat[weekday] = stat[weekday] + delta
        else:
            stat[weekday] = delta
        td += delta
    return td

def timedelta_creating(i, timetable):
    time = timetable[i].time.split('-')
    time1 = datetime.strptime(time[0], '%H:%M')
    if len(time) > 1:
        time2 = datetime.strptime(time[1], '%H:%M')
    else:
        time2 = datetime.strptime(timetable[i + 1].time.split('-')[0], '%H:%M')
    delta = time2 - time1
    return delta


def subject_time(timetable):
    d = {}
    for i in range(len(timetable)):
        delta = timedelta_creating(i, timetable)
        subj = timetable[i].subject
        if subj in d:
            d[subj] += delta.total_seconds()
        else:
            d[subj] = delta.total_seconds()

    for k, v in sorted(d.items(), key=lambda x: x[1]):
        yield k, int(v // 3600), int(v % 3600 // 60)


def teachers_hours(timetable):
    teachers_dict = {}
    for i in range(len(timetable)):
        delta = timedelta_creating(i, timetable)
        teacher = timetable[i].teacher
        if teacher in teachers_dict:
            teachers_dict[teacher] += delta.total_seconds()
        else:
            teachers_dict[teacher] = delta.total_seconds()
    for k, v in sorted(teachers_dict.items()):
        hours = int(v // 3600)
        minutes = int(v % 3600 // 60)
        h_ending = m_ending = ''
        if hours % 10 != 1 or hours % 11 == 0:
            h_ending = 's'
        if minutes % 10 != 1 or minutes % 11 == 0:
            m_ending = 's'
        yield (k, hours, h_ending, minutes, m_ending)



print("Общее время учебы: %s" % hours_per_week(timetable))
print()
for subject in subject_time(timetable):
    print("Subject %s takes %shrs %smin per week" % (subject[0], subject[1], subject[2]))
for teachers in teachers_hours(timetable):
	print('%s has to suffer from me for %s hour%s %s minute%s per week' %(teachers[0], teachers[1], teachers[2], teachers[3], teachers[4]))

