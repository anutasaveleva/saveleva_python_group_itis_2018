import multiprocessing as mp
import re
import requests
import random

semaphore = mp.Semaphore(3)
def findMatches(regex, response):
    arr = []
    for item in re.finditer(regex,response.text):
        arr.append(item.group(1))
    return arr

def downloadFiles(link):
    name = link.split('/')[-1]
    semaphore.acquire()
    with open (name, 'wb') as f:
        f.write(requests.get(link).content)
    semaphore.release()


if __name__=="__main__":
    r = requests.get("https://en.wikipedia.org/wiki/London")
    s = r"href=\"(http://[^\"]+[.]pdf)\""

    mp.freeze_support()

    for i in findMatches(s, r):
        t1 = mp.Process(target=downloadFiles(i))


