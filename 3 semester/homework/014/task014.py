import requests
import re


def download_files(link, ext):
    response = requests.get(link)
    reg = r"href=\"(http://[^\"]+[.]%s)\"" % ext
    for l in re.finditer(reg, response.text):
        dlink = l.group(1)
        name = dlink.split('/')[-1]
        with open('%s' % name, 'wb') as f:
            f.write(requests.get(dlink).content)
        break


download_files("https://en.wikipedia.org/wiki/London", "pdf")
