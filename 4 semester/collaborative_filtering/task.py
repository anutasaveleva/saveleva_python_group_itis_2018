from math import sqrt
perfect_skills = ('JS', 'Spring', 'Spark', 'Ruby', 'Swift', 'Python', 'Django', 'Java', '.NET', 'Algorithms', 'Maths')
skills = ('C#', 'Java', 'Python', 'Web', 'English', 'Robotics', 'Maths')
myskills = ('C#', 'Python', 'Russian', 'English', 'Algorithms')
k = 0.6

intersection = set(skills).intersection(myskills)
outersection = set(skills).difference(myskills)
m = 2*len(intersection)/(len(skills)+ len(myskills))
m2 = len(intersection)/sqrt(len(skills)*len(myskills))

if m>k:
    print("Keep going! Dice likes the way you study! ")
else:
    print("Dice thinks you should try better. What about learning these: %s?" %', '.join(outersection))

if m2 > k:
    print("Keep going! Jakar also likes the way you study!")
else:
    print("Jakar thinks you should try better. What about learning these: %s?" %', '.join(outersection))

