from django.urls import path

import msgs.views

urlpatterns = [
    path('im/', msgs.views.im, name='im'),
    path('im/<int:id>', msgs.views.message_details)
]
