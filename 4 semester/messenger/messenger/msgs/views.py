from django.shortcuts import render
from .models import Message
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse


# Create your views here.
def im(request):
    msgs_list = Message.objects.filter(receiver=request.user).order_by('-when')
    return render(request, 'msgs/im.html', {"msgs_list": msgs_list})

def message_details(request, id):
    msg = Message.objects.get(pk=id)
    if not msg.is_read:
        msg.is_read = True
        msg.save()
    if request.method == 'POST':
        if request.POST.get('send'):
            newmsg = Message()
            newmsg.message = request.POST.get('message')
            newmsg.sender = request.user
            newmsg.receiver = msg.sender
            newmsg.is_read = False
            newmsg.save()
        return HttpResponseRedirect(reverse('im'))

    return render(request, 'msgs/mssg.html', {'user':request.user, 'message': msg})


