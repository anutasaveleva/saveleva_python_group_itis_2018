# Generated by Django 2.1.4 on 2019-02-11 13:06

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('message', models.TextField()),
                ('when', models.DateTimeField(default=django.utils.timezone.now)),
                ('is_read', models.BooleanField()),
                ('receiver', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='msgs_received', to=settings.AUTH_USER_MODEL)),
                ('sender', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='msgs_sent', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
