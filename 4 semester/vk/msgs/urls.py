from django.urls import path
import msgs.views


urlpatterns = [
    path('im/', msgs.views.im, name='im'),
    path('im/<int:id>', msgs.views.message_details),
    path('om/', msgs.views.om, name='om'),
    path('om/<int:id>', msgs.views.message_details),
    path('new/<int:id>', msgs.views.new, name='new'),

]
