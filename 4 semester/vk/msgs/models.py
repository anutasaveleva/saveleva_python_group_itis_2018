from django.db import models
from vk.settings import CUT_MSG_TEXT_LENGTH
from django.utils.timezone import now
from django.contrib.auth.models import User

# Create your models here.
class Message(models.Model):
    sender = models.ForeignKey(User, related_name="msgs_sent", on_delete=models.PROTECT)
    receiver = models.ForeignKey(User, related_name="msgs_received", on_delete=models.PROTECT)
    message = models.TextField()
    when = models.DateTimeField(default=now)
    is_read = models.BooleanField()

    def __str__(self):
        return  self.message

    def cut(self):
        if len(self.message) <= CUT_MSG_TEXT_LENGTH:
            return self.message
        return self.message[:CUT_MSG_TEXT_LENGTH] + "..."