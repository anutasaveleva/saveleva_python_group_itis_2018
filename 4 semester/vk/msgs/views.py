from django.contrib.auth.decorators import login_required
from django.forms import modelform_factory
from django.shortcuts import render
from .models import Message
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User


@login_required(login_url='login')
def im(request):
    msgs_list = Message.objects.filter(receiver=request.user).order_by('-when')
    if request.GET.get('id'):
        msgs_list = Message.objects.filter(sender__id=request.GET.get('id'))
    return render(request, 'msgs/im.html', {"msgs_list": msgs_list})


@login_required(login_url='login')
def om(request):
    msgs = Message.objects.filter(sender=request.user).order_by('-when').values('receiver__username','message','when')
    if request.method == 'POST':
        return HttpResponseRedirect(reverse('im'))
    return render(request, 'msgs/om.html', {'msgs': msgs})


@login_required(login_url='login')
def new(request, id):
    if request.method == 'POST':
        msg = Message()
        msg.message = request.POST.get('message')
        msg.sender = request.user
        msg.receiver = User.objects.get(id=id)
        msg.is_read=False
        msg.save()
        return HttpResponseRedirect(reverse('person',kwargs={'id':id}))
    form = modelform_factory(Message, fields=('message',))
    return render(request, 'msgs/newmessage.html', {'user1':request.user,
                                                    'user2': User.objects.get(pk=id), 'form':form})



@login_required(login_url='login')
def message_details(request, id):
    msg = Message.objects.get(pk=id)
    if not msg.is_read:
        msg.is_read = True
        msg.save()
    if request.method == 'POST':
        if request.POST.get('send'):
            newmsg = Message()
            newmsg.message = request.POST.get('message')
            newmsg.sender = request.user
            newmsg.receiver = msg.sender
            newmsg.is_read = False
            newmsg.save()
        return HttpResponseRedirect(reverse('im'))

    return render(request, 'msgs/mssg.html', {'user':request.user, 'message': msg})



