from django.db import models
from datetime import datetime

# Create your models here.
class Document(models.Model):
    types = (('1','text'), ('2','archive'), ('3', 'gif'),
             ('4', 'image'), ('5', 'audio'), ('6', 'video'),
             ('7','book'), ('8','other'))
    title = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now=datetime.now)
    type = models.CharField(max_length=2, choices=types)
    file = models.FileField(upload_to='uploads/docs/')

    def __str__(self):
        return self.title
