# Generated by Django 2.1.4 on 2019-03-04 12:03

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('date', models.DateTimeField(auto_now=True)),
                ('type', models.CharField(choices=[('1', 'text'), ('2', 'archive'), ('3', 'gif'), ('4', 'image'), ('5', 'audio'), ('6', 'video'), ('7', 'book'), ('8', 'other')], max_length=2)),
                ('file', models.FileField(upload_to='uploads/docs/')),
            ],
        ),
    ]
