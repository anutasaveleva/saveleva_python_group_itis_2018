from django.db import models
from datetime import datetime
from datetime import datetime

# Create your models here.
class Video(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    date = models.DateTimeField(auto_now=datetime.now)
    vid = models.FileField(upload_to='uploads/videos/')

    def __str__(self):
        return  '%s %s' %(self.name, self.date.date())