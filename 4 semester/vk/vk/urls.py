"""vk URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.staticfiles.urls import static
from django.urls import path
from django.urls import include

import pages
from pages.views import PostList, ProfilePageViewSet, UserViewSet, ImageViewSet, LatestPosts
from rest_framework import routers

from vk import settings

router = routers.DefaultRouter()
router.register(r'profiles', ProfilePageViewSet)
router.register(r'users', UserViewSet)
router.register(r'images', ImageViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('messages/', include('msgs.urls')),
    path('', include('pages.urls')),
    path('feed/', login_required(PostList.as_view(template_name='pages/posts.html'), login_url='login'), name='feed' ),
    path('feed/latest', login_required(LatestPosts.as_view(template_name='pages/posts.html'),login_url='login'), name='latest_posts' ),
    path('feed/like/', pages.views.like, name='like'),
    path('rest/', include(router.urls)),
    path(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('', include('login.urls')),
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
