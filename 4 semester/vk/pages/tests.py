from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.
from django.urls import reverse


class PagesTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_feed(self):
        response = self.client.get('/feed/')
        self.assertEqual(response.status_code, 200)

class LoginTest(TestCase):
    def setUp(self):
        self.client = Client()
        User.objects.create_user(username="user", password="user")

    def test_login(self):
        login_stat = self.client.login(username="user", password="user")
        self.assertEqual(login_stat, True)
        response = self.client.get("/feed")
        self.assertEqual(response.status_code, 301)
        response = self.client.get(reverse("login"))
        self.assertEqual(response.status_code, 302)


