import datetime
from django.contrib.auth.decorators import permission_required, login_required
from django.core import serializers
from django.db.models import F, Q
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from .models import Post, ProfilePage
from photos.models import Photo
from .forms import Formp, ImgForm
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from django.views.generic import ListView, TemplateView
from rest_framework import viewsets
from .serializers import ProfilePageSerializer, UserSerializer, ImageSerializer


class Person(TemplateView):
    template_name = 'pages/personposts.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        id=self.kwargs['id']
        auth_user = self.request.user
        user = User.objects.get(pk=id)
        person = ProfilePage.objects.get(pk=id)
        p = person.wall
        context['auth_user'] = auth_user
        context['user'] = user
        context['is_owner'] = False
        context['person'] = person
        context['p'] = p
        context['object_list'] = Post.objects.filter(where_posted=p).order_by('-date')
        context['is_owner'] = auth_user == user
        context['is_friend'] = are_friends(auth_user, user) or  auth_user == user
        context['post'] = ImgForm()
        return context

def are_friends(user1, user2):
    friendslist = ProfilePage.objects.get(user=user1).subscriptions.all()
    profile = ProfilePage.objects.get(user=user2)
    return profile in friendslist


def update_status(request, id):
    if request.POST.get('send'):
        p = ProfilePage.objects.get(id=id)
        p.status = request.POST.get('text')
        p.save()
        return redirect('person', id=id)



def friends(request, id,):
    friend1 = ProfilePage.objects.get(id=id)
    friend2 = ProfilePage.objects.get(user=request.user)
    if friend1 in friend2.subscriptions.all():
        friend2.subscriptions.remove(friend1)
    else:
        friend1.subscriptions.add(friend2)
    friend1.save()
    friend2.save()
    return redirect('person', id=id)

def getJSON(request, id):
    person = ProfilePage.objects.all()
    return serializers.serialize('json', person)

@login_required(login_url="login")
def showfriends(request):
    object_list = ProfilePage.objects.get(user=request.user).subscriptions.all()
    return render(request, 'pages/friends.html', {'object_list': object_list})

class PostList(ListView):
    model = Post

    def get_queryset(self):
        word = self.request.GET.get('word')
        if word:
            return Post.objects.filter(text__icontains=word).order_by('-date')
        return Post.objects.all().order_by('-date')

class LatestPosts(ListView):
    model = Post

    def get_queryset(self, *args, **kwargs):
        today = datetime.date.today()
        yesterday = today - datetime.timedelta(days=1)
        queryset_filtered = Post.objects.filter(date__gt=yesterday)
        print(queryset_filtered)
        return queryset_filtered

class ProfilePageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = ProfilePage.objects.all()
    serializer_class = ProfilePageSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ImageViewSet(viewsets.ModelViewSet):
    queryset = Photo.objects.all()
    serializer_class = ImageSerializer

@login_required(login_url="login")
def posts(request):
    word = request.GET.get('word')
    if word:
        postList = Post.objects.filter(Q(text__contains=word))
    else:
        postList = Post.objects.all()
    return render(request, 'pages/posts.html', {'object_list': postList})

@permission_required('pages.delete_post', raise_exception=True)
def post_del(request, id, post_id):
    Post.objects.get(id=post_id).delete()
    return redirect('/%s/' % id)

@permission_required('pages.add_post', raise_exception=True)
def createpost(request):
    if request.method == 'POST':
        post = Post()
        post.text = request.POST.get('text')
        post.who_posted = ProfilePage.objects.get(user=request.user)
        where_id = request.POST['where_posted']
        page = ProfilePage.objects.get(id=where_id)
        post.where_posted = page.wall
        post.save()
        photo = ImgForm(request.POST, request.FILES)
        if photo.is_valid():
            p = photo.save()
            post.photo_att = p
        post.save()
    return redirect('/%s/' % where_id)


@login_required(login_url='login')
def form(request):
    f = Formp(request.POST)
    return render(request, 'pages/form.html', {'form': f})


def save(request):
    f = Formp(request.POST)
    newa = f.save()
    return HttpResponse("The form is saved")

@csrf_exempt
def like(request):
    if request.method == "POST":
        post = Post.objects.get(id=request.POST.get('post_id'))
        print(post)
        if post.like.filter(user__id=request.user.id):
            post.like.remove(ProfilePage.objects.get(user=request.user))
            post.like_counter = F('like_counter') - 1
            b=True
        else:
            post.like.add(ProfilePage.objects.get(user=request.user))
            post.like_counter = F('like_counter') + 1
            b=False
        print(b)
        post.save()
        return JsonResponse({'liked': b})