from django.db import models
from datetime import datetime
from audios.models import Audio
from videos.models import Video
from photos.models import Album, Photo
from docs.models import Document
from walls.models import Wall
from django.contrib.auth.models import User

# Create your models here.
class Page(models.Model):
    creation_date = models.DateTimeField(auto_now=datetime.now)
    url = models.URLField(blank=True)
    status = models.CharField(max_length=140, blank=True)
    website = models.URLField(blank=True)
    album = models.ForeignKey(Album, blank=True, null=True, on_delete=models.PROTECT)
    music = models.ManyToManyField(Audio, blank=True, null=True)
    videos = models.ManyToManyField(Video, blank=True, null=True)
    page_pic = models.ForeignKey(Photo, blank=True, null=True, on_delete=models.PROTECT)
    wall = models.ForeignKey(Wall, null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        abstract = True

class VerifiedMixin(models.Model):
    isVerified = models.BooleanField(default=False)

    class Meta:
        abstract = True

class EstDateMixin(models.Model):
    est_date = models.DateField(default=datetime.now)

    class Meta:
        abstract = True

class DocumentsMixin(models.Model):
    docs = models.ForeignKey(Document, blank=True, null=True, on_delete=models.SET_NULL)

    class Meta:
        abstract = True

class ExtraInfoMixin(models.Model):
    interests = models.TextField(blank=True)
    activities = models.TextField(blank=True)
    favourite_music = models.TextField(blank=True)
    favourite_films = models.TextField(blank=True)
    favourite_books = models.TextField(blank=True)
    favourite_quotes = models.TextField(blank=True)
    religion = models.CharField(max_length=40, blank=True)
    inspired_by = models.TextField(blank=True)

    class Meta:
        abstract = True


class ProfilePage(DocumentsMixin, VerifiedMixin, ExtraInfoMixin, Page):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    first_name = models.CharField(max_length=40)
    middle_name = models.CharField(max_length=40, blank=True, null=True)
    last_name = models.CharField(max_length=40)
    date_of_birth = models.DateField()
    subscriptions = models.ManyToManyField('self', blank=True, null=True)

    def __str__(self):
        return self.first_name+self.last_name

class Contact(models.Model):
    occupation = models.CharField(max_length=100, blank=True)
    phone = models.CharField(max_length=20, blank=True)
    email = models.CharField(max_length=40, blank=True)
    link = models.ForeignKey(ProfilePage, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return  self.occupation

class Community(Page):
    title = models.CharField(max_length=140)
    subscribers = models.ManyToManyField(ProfilePage, blank=True,  related_name='subscribers')
    information = models.TextField(blank=True)
    address = models.TextField(blank=True)
    banner = models.ImageField(blank=True, upload_to='uploads/banners/')
    admins = models.ManyToManyField(ProfilePage, blank=True, null=True,  related_name='admins')
    contacts = models.ManyToManyField(Contact, blank=True)

    def __str__(self):
        return  self.title


class Group(Community, EstDateMixin, DocumentsMixin):
    statuses = (('o', 'open'), ('c', 'closed'), ('p', 'private'))
    open_status = models.CharField(max_length=1, choices=statuses, verbose_name='Group type')


class Meeting(Community):
    start_date = models.DateTimeField(auto_now=datetime.now)
    end_date = models.DateTimeField(auto_now=start_date)
    probable = models.ManyToManyField(ProfilePage, blank=True, related_name='probable')
    exact = models.ManyToManyField(ProfilePage, blank=True, related_name='exact')



class PublicPage(Community, VerifiedMixin, EstDateMixin):
    meetings = models.ForeignKey(Meeting, blank=True, null=True, on_delete=models.PROTECT)

class Post(models.Model):
    date = models.DateTimeField(default=datetime.now)
    text = models.TextField()
    who_posted = models.ForeignKey(ProfilePage, on_delete=models.DO_NOTHING, related_name='all_posts_on_walls')
    where_posted = models.ForeignKey(Wall, on_delete=models.CASCADE, related_name='wallposts')
    photo_att = models.ForeignKey(Photo, blank=True, null=True, on_delete=models.PROTECT)
    album_att = models.ForeignKey(Album, blank=True, null=True, on_delete=models.PROTECT)
    video_att = models.ForeignKey(Video, blank=True, null=True, on_delete=models.PROTECT)
    audio_att = models.ForeignKey(Audio, blank=True, null=True, on_delete=models.PROTECT)
    doc_att = models.ForeignKey(Document, blank=True, null=True, on_delete=models.PROTECT)
    like = models.ManyToManyField(ProfilePage, related_name='likes', blank=True)
    like_counter = models.IntegerField(default=0)

    def __str__(self):
        return self.text[:50]