from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator

from photos.models import Photo
from .models import Post

COURSE_CHOICES=(('1','I'),('2','II'),('3','III'), ('4','IV'))
class Formp(forms.Form):
    name=forms.CharField(max_length=40)
    surname=forms.CharField(max_length=40)
    course=forms.CharField(max_length=3,
                           widget=forms.Select(choices=COURSE_CHOICES))
    text=forms.CharField(label='Your opinion',widget=forms.Textarea)
    phone_number=forms.CharField(max_length=12,
                                 validators=[RegexValidator
                                 (r"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$"
                                                           ,"Wrong phone number!")])
    email=forms.EmailField()

    def save(self):
        data = self.cleaned_data


class ImgForm(forms.ModelForm):
    img = forms.FileField(required=False)

    class Meta:
        model=Photo
        fields=['img']




