from django.contrib.auth.decorators import login_required
from django.urls import path
from . import views

urlpatterns = [
    path('<int:id>/', login_required(views.Person.as_view(),login_url='login'), name='person'),
    path('<int:id>/delete/<int:post_id>', views.post_del, name='post_del'),
    path('form/', views.form),
    path('save/', views.save),
    path('<int:id>/upd/', views.update_status, name='update_status'),
    path('createpost/', views.createpost, name='createpost'),
    path('friends/', views.showfriends),
    path('<int:id>/friend', views.friends , name='friend')
]