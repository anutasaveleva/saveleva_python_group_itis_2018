from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand

from msgs.models import Message


class Command(BaseCommand):
    def handle(self, *args, **options):
        addGroup()

def addGroup():
    new_group, created = Group.objects.get_or_create(name='hackers')
    msgs_delete_message_perm = Permission.objects.get(name='Can delete message')
    new_group.permissions.add(msgs_delete_message_perm)
