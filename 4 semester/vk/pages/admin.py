from django.contrib import admin
from .models import ProfilePage, Group, Meeting, PublicPage, Post, Contact
# Register your models here.
admin.site.register(ProfilePage)
admin.site.register(PublicPage)
admin.site.register(Group)
admin.site.register(Meeting)
admin.site.register(Post)
admin.site.register(Contact)