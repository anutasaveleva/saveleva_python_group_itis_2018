from rest_framework import serializers
from .models import ProfilePage, Post, Photo
from django.contrib.auth.models import User

class ProfilePageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model=ProfilePage
        fields=('creation_date', 'url', 'status','website', 'page_pic',
                'first_name', 'middle_name', 'last_name', 'date_of_birth')

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model=Photo
        fields=('img', 'description', 'date')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields=('username','date_joined')
