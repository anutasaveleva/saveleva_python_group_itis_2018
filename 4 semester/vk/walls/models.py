from django.db import models
from photos.models import Photo, Album
from videos.models import Video
from audios.models import Audio
from docs.models import Document
from datetime import datetime

class Wall(models.Model):
    choices = (('O', 'Opened'), ('C', 'Closed'), ('L', 'Limited'), ('S', 'Switched Off'))
    type = models.CharField(max_length=1, choices=choices)
    whose = models.TextField(null=True, blank=True)


