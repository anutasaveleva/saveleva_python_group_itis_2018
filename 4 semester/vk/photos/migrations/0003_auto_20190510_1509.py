# Generated by Django 2.1.4 on 2019-05-10 12:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('photos', '0002_auto_20190310_1826'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='img',
            field=models.ImageField(upload_to='profile/pictures'),
        ),
    ]
