from django.db import models
from datetime import datetime
# Create your models here.
from django.utils.timezone import now


class Album(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)

    def __str__(self):
        return  self.name

class Photo(models.Model):
    img = models.ImageField(upload_to='profile\pictures', verbose_name='Image')
    description = models.TextField(blank=True)
    date = models.DateTimeField(auto_now=now)
    album = models.ForeignKey(Album, on_delete=models.CASCADE, null=True)


