from django.db import models

# Create your models here.
class Audio(models.Model):
    genres = (('C', 'Classic'), ('R', 'Rock'), ('I', 'Indie'), ('E', 'Electro'), ('O','Other'))
    artist = models.CharField(max_length=100)
    title = models.CharField(max_length=140)
    lyrics = models.TextField(blank=True)
    genre = models.CharField(max_length=2, choices=genres, blank=True)
    file = models.FileField(upload_to='uploads/audios/')

    def __str__(self):
        return '%s - %s' %(self.artist, self.title)