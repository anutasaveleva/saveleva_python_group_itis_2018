from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib import auth
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login


def login(request):
    args = {}
    if request.method == 'POST':
        if request.session.test_cookie_worked():
            request.session.delete_test_cookie()
            return HttpResponse("You're logged in.")
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            auth_login(request, user)
            request.session.set_test_cookie()
            return redirect('/feed/')
        else:
            args['login_error'] = "Неверный логин или пароль"
            return render(request, 'login/login.html', args)
    else:
        user = auth.get_user(request)
        if user.is_authenticated:
            return redirect('/' + str(user.id))
        return render(request, 'login/login.html', args)


def logout(request):
    if auth.get_user(request).is_authenticated:
        auth_logout(request)
    return render(request, 'login/logout.html')

