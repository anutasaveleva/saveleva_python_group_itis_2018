from django.shortcuts import render
from filtering.models import SkillPerson, Person, Skill
from django.http import HttpResponse
from math import sqrt

# Create your views here.
def compare(request, id1, id2):
    #args = request.GET['id']
    per1 = Person.objects.get(id=id1)
    per2 = Person.objects.get(id=id2)

    if per1 and per2:
        p1 = SkillPerson.objects.filter(person=per1).values_list('skill', flat=True)
        p2 = SkillPerson.objects.filter(person=per2).values_list('skill', flat=True)
        k = 0.6

        intersection = set(p1).intersection(p2)
        outersection = set(p1).difference(p2)
        skillsum = len(p1) + len(p2)
        skillmult = len(p1) * len(p2)
        if skillsum * skillmult == 0:
            return HttpResponse("One or more workers don't have any skills")
        m = 2 * len(intersection) / skillsum
        m2 = len(intersection) / sqrt(skillmult)

        if m > k:
            return HttpResponse("Keep going! Dice thinks %s and %s can work together " %(per1.name, per2.name))
        else:
            difskills = Skill.objects.filter(id__in=outersection).values_list('name', flat=True)
            return HttpResponse("You have differences in these skills: %s" %(", ".join(difskills)))

    else:
        return 'Error in request'
