from django.apps import AppConfig


class ComparingConfig(AppConfig):
    name = 'comparing'
