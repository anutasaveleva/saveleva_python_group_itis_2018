from django.db import models

# Create your models here.
class Person(models.Model):
    name = models.CharField(max_length=40)
    surname = models.CharField(max_length=40)

    def __str__(self):
        return self.name + self.surname

class Skill(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name

class SkillPerson(models.Model):
    person = models.ForeignKey(Person,on_delete=models.CASCADE)
    skill = models.ForeignKey(Skill, on_delete=models.CASCADE)
    def __str__(self):
        return self.person.name+self.skill.name