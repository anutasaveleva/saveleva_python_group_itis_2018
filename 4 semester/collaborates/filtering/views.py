from django.shortcuts import render
from django.http import HttpResponse
from .models import Person, SkillPerson, Skill

# Create your views here.
def recommendations(request, id):
    if request.method == "POST":
        context = {'skills':Skill.objects.all }
        if request.POST.get("edit"):
            return HttpResponse(render(request, 'filtering/choose.html', context))
        if request.POST.get("compare"):


    personx = Person.objects.get(id=id)
    skills = SkillPerson.objects.filter(person=personx)
    context = {'skills': skills, 'person': personx}
    return render(request,'filtering/recommends.html', context)

def findclosest(id):
