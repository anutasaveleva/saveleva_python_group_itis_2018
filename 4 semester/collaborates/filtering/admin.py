from django.contrib import admin
from .models import Skill, Person, SkillPerson
# Register your models here.
admin.site.register(Skill)
admin.site.register(Person)
admin.site.register(SkillPerson)